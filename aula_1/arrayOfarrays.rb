# Crie uma função que dado um array de arrays, faça a soma, subtração, multiplicação e divisão (que retorne em ponto flutuante)
# Ex: 
# Entrada: [[2, 5, 7], [3, 2, 4, 10], [1, 2, 3]]
# Saída: Soma: [[14],[19],[6]]
# Saída: Subtração: [[-10],[-13],[-4]]
# Saída: Multiplicação: [[70],[240],[6]]
# Saída: Divisão: [[0.057],[0.037],[0.166]]
def arrayOfarrays matrix
    array_soma = []
    array_subtracao = []
    array_multiplicacao = []
    array_divisao = []
    matrix.each do |submatrix|
        primeira_vez = true
        soma = submatrix[0]
        subtracao = submatrix[0]
        multiplicacao = submatrix[0]
        divisao = submatrix[0].to_f
        submatrix.each do |integer|
          if(primeira_vez == false)
            soma += integer
            subtracao -= integer
            multiplicacao *= integer
            divisao /= integer
          end
          primeira_vez = false
        end
        primeira_vez = true
        array_soma.push(soma) 
        array_subtracao.push(subtracao) 
        array_multiplicacao.push(multiplicacao)
        array_divisao.push(divisao) 
    end
    puts "Soma: #{array_soma}"
    puts "Subtração: #{array_subtracao}"
    puts "Multiplicação: #{array_multiplicacao}"
    puts "Divisão: #{array_divisao}"
end
arrayOfarrays [[2, 5, 7], [3, 2, 4, 10], [1, 2, 3]]

