# Crie uma função, que dado um hash como parâmetro, e retorne um array com todos os valores que estão no hash elevados ao quadrado.
def hash2arraysqr hashTable
    arr = []
    hashTable.each_key do |chave|
        arr.push(hashTable[chave]**2)
    end
    return arr
end
hashTable = {:chave1 => 5, :chave2 => 30, :chave3 => 20}
array = hash2arraysqr hashTable
print array