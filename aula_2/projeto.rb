class Projeto
    attr_accessor :codigo
    attr_accessor :cliente
    attr_accessor :nome
    attr_accessor :departamento
    attr_accessor :desenvolvedores
    def initialize (codigo, cliente, nome, departamento, desenvolvedores)
        self.codigo = codigo
        self.cliente = cliente
        self.nome = nome
        self.departamento = departamento
        self.desenvolvedores = desenvolvedores
    end
    def listarDevs (desenvolvedor)
        desenvolvedores.each do |desenvolvedor|
            if desenvolvedor.cargo == "desenvolvedor"
                puts desenvolvedor.nome
            end
        end
    end
    def adicionarDev (desenvolvedor)
        if desenvolvedor.cargo == 'desenvolvedor'
            desenvolvedores.push(desenvolvedor)
        end
    end
end