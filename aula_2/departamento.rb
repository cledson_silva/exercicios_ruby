class Departamento
    attr_accessor :codigo
    attr_accessor :gerente
    attr_accessor :nome
    attr_accessor :funcionarios
    attr_accessor :localizacao
    attr_accessor :projetos
    def initialize (codigo, gerente, nome, funcionarios, localizacao, projetos)
        self.codigo = codigo
        self.gerente = gerente
        self.nome = nome
        self.funcionarios = funcionarios
        self.localizacao = localizacao
        self.projetos = projetos
    end
    def adicionarFuncionario (funcionario)
        funcionarios.push(funcionario)
    end
    def retirarFuncionario (funcionario)
        funcionarios.pop(funcionario)
    end
    def listarFuncionarios
        funcionarios.each do |funcionario|
            puts funcionario.nome
        end
    end
    def adicionarProjeto (projeto)
        projetos.push(projeto)
    end
end